<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tmc');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'DuSo^|Sk6NLXC#3RVx#$*h-RtTE`juKW+Er}bq<4_uX&,f?_}aTvyfqhNl&(-]r+');
define('SECURE_AUTH_KEY',  '*xYJvU$_d`)AV-vs_g>vHx|!az8<OAfwXw~]UlCP F`8+Aj$-$k6ylPbB:Ogt[hN');
define('LOGGED_IN_KEY',    '^|+[Vpqv-|qCS:tp.NoaGXJSS-Q|aKs2,~];-x?GhG{e-u]hZTjtu+-l+wman2,W');
define('NONCE_KEY',        'DP;s+b0DUA-UW.QeZ 2b1X$v$)m(>o!gx<Gl!rxG,m+fn.5D7GjX,ybQ/lY*fu*I');
define('AUTH_SALT',        'q+p:|1ytN>(-uxGC3SA[aDwU#C_6zO|SW`TDR@.b/>u0C=sS~.kBqd-2$Y78%cv{');
define('SECURE_AUTH_SALT', 'g+*VUG.5A/]gu:Kqm]U#s8H$y`%p9T)s`jp{G=OTSScv3%AU*_=T$5|>9:D)IvAZ');
define('LOGGED_IN_SALT',   '<V$MF~okyUAg_B*nZ3bGN=!=tYfG3.AHm1CDu=7t+sD/U{<h8b#/_DBShZc}uAs+');
define('NONCE_SALT',       ']?9*ol/(!^11^bO%_8fP8](/1=:|Xn0.Eh%tYFphp7]ef%g`028_~)IbYx9VG;N%');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
