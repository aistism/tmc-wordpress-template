    <?php if (isset($contact_page) && !empty($contact_page)) : ?>
        <footer>
            <?php if (isset($fields['slogan']) && !empty($fields['slogan'])) : ?>
            <div class="slogan"><?php echo $fields['slogan']; ?></div>
            <?php endif; ?>
            <div class="social-links">
            <?php if (isset($fields['instagram_link']) && !empty($fields['instagram_link'])) : ?>
            <a href="<?php echo $fields['instagram_link']; ?>" target="_blank" class="instagram" title="Instagram"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/instagram.svg" alt="instagram"></a>
            <?php endif; ?>
            <?php if (isset($fields['facebook_link']) && !empty($fields['facebook_link'])) : ?>
            <a href="<?php echo $fields['facebook_link']; ?>" target="_blank" class="facebook" title="Facebook"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/facebook.svg" alt="facebook"></a>
            <?php endif; ?>
            <?php if (isset($fields['twitter_link']) && !empty($fields['twitter_link'])) : ?>
            <a href="<?php echo $fields['twitter_link']; ?>" target="_blank" class="twitter" title="Twitter"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/twitter.svg" alt="twitter"></a>            
            <?php endif; ?>
            </div>
        </footer>
        </div>
    </section>
    <?php endif; ?>

    <div class="hidden" id="prefabs">
        <article class="creamest-ideas-item hidden">
        <figure>
            <img class="placeholder" src="" width="625" height="420" alt="">
            <figcaption></figcaption>
        </figure>
        <div></div>
        </article>
    </div>

    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/scripts.js" type="text/javascript"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/carousel.js" type="text/javascript"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/libs/unveil/jquery.unveil.js" type="text/javascript"></script>
    <?php wp_footer(); ?>

    </body>
</html>