<!DOCTYPE html>
<html>
    <head>
        <title><?php echo bloginfo('site_title'); ?></title>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/libs/jquery/jquery.js" type="text/javascript"></script>

        <?php wp_head(); ?>
    </head>
    <body>
    <div id="subscribe-form">
        <?php $subscribe_nonce = wp_create_nonce("subscribe_email_nonce"); ?>
        <div class="page-wrap">
        <form>
            <input id="nonce" type="hidden" value="<?php echo $subscribe_nonce; ?>">
            <input placeholder="Your e-mail address" id="email" type="text" name="email">
            <button>Subscribe</button>
        </form>
        <div id="subscribe-close"></div>
        </div>
    </div>

    <header id="header">
        <div class="page-wrap">
            <div id="menu"><i></i></div>
            <div id="logo"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.svg" alt=""></div>
            <a class="menu-subscribe" href="#menu-subscribe">Subscribe</a> 
            
            <?php

            $args = array(
                'order'                  => 'ASC',
                'orderby'                => 'menu_order',
                'post_type'              => 'nav_menu_item',
                'post_status'            => 'publish',
                'output_key'             => 'menu_order',
            );

            $menu_items = wp_get_nav_menu_items( 'Main Menu', $args );        
            ?>
            <nav class="menu-primary">
                <ul>

                <?php foreach($menu_items as $item) : ?>
                    <li><a href="#<?php echo strtolower(str_replace(' ', '-', $item->title)); ?>"><?php echo $item->title; ?></a></li>
                <?php endforeach; ?>

                <ul>
            </nav>

        </div>
    </header>