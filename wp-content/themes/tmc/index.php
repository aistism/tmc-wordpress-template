<?php get_header(); ?>
    
    <?php
        $about_page = get_posts(array(
            'post_type' => 'page',
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page-about.php'
        ));
        $about_page_fields = array();
        if (!empty($about_page)) {
            $about_page_fields = get_fields($about_page[0]->ID);
        }

        if (isset($about_page_fields['main_slide_show']) && !empty($about_page_fields['main_slide_show'])) {
            $main_slides = $about_page_fields['main_slide_show'];
        }

        if (isset($about_page_fields['sub_slide_show_first_image']) && !empty($about_page_fields['sub_slide_show_first_image'])) {
            $sub_slides_first = $about_page_fields['sub_slide_show_first_image'];
        }

        if (isset($about_page_fields['sub_slide_show_second_image']) && !empty($about_page_fields['sub_slide_show_second_image'])) {
            $sub_slides_second = $about_page_fields['sub_slide_show_second_image'];
        }
    ?>
    
    <?php if (!empty($about_page)) : ?>
	<section id="splash">
	    <div class="page-wrap">
		<div class="slideshow-items slideshow-temp">
            <?php if (!empty($main_slides)) : ?>
                <?php $first = 0; ?>
                <?php foreach ($main_slides as $slide) : ?>

                    <?php 
                    $class = ''; 
                    if ($first == 0) {
                        $class = 'active';
                    }
                    ?>

                    <?php $imageSrc = wp_get_attachment_image_src( $slide['image']['id'], '850x550'); ?>
                    <img class="<?php echo $class; ?>" width="<?php echo $imageSrc[1]; ?>" height="<?php echo $imageSrc[2]; ?>" src="data:image/gif;base64,R0lGODlhIwAWAIAAAP///wAAACH5BAEAAAAALAAAAAAjABYAAAIahI+py+0Po5y02ouz3rz7D4biSJbmiaZqWAAAOw==" data-src="<?php echo $imageSrc[0]; ?>">
                    <?php $first++; ?>
                <?php endforeach; ?>

            <?php endif; ?>
		</div>
		<div class="slideshow-subitems">

            <?php if (!empty($sub_slides_first)) : ?>
                <?php $imageSrc = wp_get_attachment_image_src( $sub_slides_first['id'], '400x250'); ?>
              <div class="slideshow-subitem first"><img class="responsive" src="<?php echo $imageSrc[0]; ?>"  width="<?php echo $imageSrc[1]; ?>" height="<?php echo $imageSrc[2]; ?>"></div>
            <?php endif; ?>

            <?php if (!empty($sub_slides_second)) : ?>
                <?php $imageSrc = wp_get_attachment_image_src( $sub_slides_second['id'], '400x250'); ?>
		      <div class="slideshow-subitem second"><img class="responsive" src="<?php echo $imageSrc[0]; ?>"  width="<?php echo $imageSrc[1]; ?>" height="<?php echo $imageSrc[2]; ?>"></div>
            <?php endif; ?>


		</div>
	    </div>
	</section>

    <section id="about-us">
        <div class="page-wrap">
        <header>
            <h2><span><?php echo $about_page[0]->post_title; ?></span></h2>
        </header>
        
        <article class="about-us">
            <figure>

            <?php if (isset($about_page_fields['about_us_main_image']) && !empty($about_page_fields['about_us_main_image'])) : ?>
            
            <?php $about_main_image = wp_get_attachment_image_src( $about_page_fields['about_us_main_image']['id'], '625x420');?>
			<img src="<?php echo $about_main_image[0]; ?>" width="<?php echo $about_main_image[1]; ?>" height="<?php echo $about_main_image[2]; ?>" alt="">
		    
            <?php endif; ?>

            </figure>
            <?php if (isset($about_page_fields['content'])) : ?>
                <div><?php echo $about_page_fields['content']; ?></div>
            <?php endif;?>
		</article>
	    </div>
	</section>

    	<?php if (isset($about_page_fields['persons_list']) && !empty($about_page_fields['persons_list'])) : ?>
        	<section id="persons">
        	    <ul class="page-wrap">

                <?php foreach ($about_page_fields['persons_list'] as $person) : ?>
        		<li class="person">
                    <?php $personPic = wp_get_attachment_image_src( $person['picture']['id'], '290x300'); ?>
        		    <img src="<?php echo $personPic[0]; ?>" width="<?php echo $personPic[1]; ?>" height="<?php echo $personPic[2]; ?>" alt="">
        		    <div class="name"><?php echo $person['name']; ?></div>
        		    <div class="description"><?php echo $person['description']; ?></div>
        		    <div class="contact"><a href="<?php echo $person['email']; ?>"><?php echo $person['email']; ?></a></div>
        		</li>
                <?php endforeach; ?>

        		<li class="person"></li>
        		<li class="person"></li>
        	    </ul>
        	</section>
	
        <?php endif; ?>
    <?php endif; ?>
	

    <?php
        $baltic_page = get_posts(array(
            'post_type' => 'page',
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page-baltics.php'
        ));
    ?>

    <?php if (!empty($baltic_page)) : ?>
    <?php 
        $baltics_fields = get_fields($baltic_page[0]->ID);

        if (isset($baltics_fields['slides']) && !empty($baltics_fields['slides'])) {
            $slides = array();

            foreach ($baltics_fields['slides'] as $slide) {
                $imageSrc = wp_get_attachment_image_src( $slide['image']['id'], '850x565');
                $slide = array(
                    'image' => $imageSrc[0],
                    'paragraph' => $slide['content']
                );
                array_push($slides, $slide);
            }
        }
    ?>
    <script type="text/javascript">
    var MicestBaltics = <?php echo json_encode($slides); ?>;
    </script>
    
	<section id="micest-baltics">
	    <div class="page-wrap">
		<header>
		    <h2><span><?php echo $baltic_page[0]->post_title; ?></span></h2>
		</header>
		
		<div class="micest-baltics">
		    
		    <article class="micest-baltics-item">
			<figure>
			    <img id="micest-baltics-image" class="placeholder" src="" alt="" width="850" height="565">
			</figure>
			<div class="micest-baltics-navigation">
			    <ul>
				<li><a id="micest-baltics-navigation-previous" href="#micest-baltics-previous">Previous</a></li>
				<li><a id="micest-baltics-navigation-next" href="#micest-baltics-next">Next</a></li>
			    </ul>
			</div>			
			<div class="micest-baltics-item-description"></div>
			<script>$(function(){ addMicestBaltics(); });</script>		
		    </article>
		</div>
	    </div>
	</section>
	<?php endif; ?>
    
    <?php $nonce = wp_create_nonce("post_load_nonce"); ?>

    <?php
        $posts = get_posts(array(
            'post_type'        => 'post',
            'posts_per_page'   => 2,
            'paged'            => 1,
            'post_status'      => 'publish',
        ));
    ?>

	<section id="creamiest-ideas">
	    <div class="page-wrap">
		<header>
		    <h2><span>Creamiest ideas</span></h2>
		</header>
		
		<div class="creamest-ideas">
		    <?php foreach ($posts as $post) : ?>
                <?php $post_fields = get_fields($post->ID); ?>
    		    <article class="creamest-ideas-item">
    			<figure>
                    <?php if (isset($post_fields['post_image']) && !empty($post_fields['post_image'])) : ?>
                        <?php $imageSrc = wp_get_attachment_image_src( $post_fields['post_image']['id'], '625x420'); ?>
    			        <img class="placeholder" src="<?php echo $imageSrc[0]; ?>" width="<?php echo $imageSrc[1]; ?>" height="<?php echo $imageSrc[2]; ?>" alt="">
                    <?php endif; ?>
    			    <figcaption><?php echo $post->post_title; ?></figcaption>
    			</figure>
    			<div><?php echo apply_filters ("the_content", $post->post_content); ?></div>
    		    </article>
            <?php endforeach; ?>
		    
		</div>
		
		<button id="creamest-ideas-more" data-page="2" data-nonce="<?php echo $nonce; ?>">Show more</button>
	    </div>
	</section>

    <?php
        $contact_page = get_posts(array(
            'post_type' => 'page',
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page-find.php'
        ));
    ?>

    <?php if (isset($contact_page) && !empty($contact_page)) : ?>
    <?php $fields = get_fields($contact_page[0]->ID); ?>
	<section id="find-us">
	    <div class="page-wrap">
		<header>
		    <h2><span><?php echo $contact_page[0]->post_title; ?></span></h2>
		</header>
		
		<div class="find-us">
		    <div class="contacts-wrap">
    			<div class="contacts">

                <?php if (isset($fields['main_image']) && !empty($fields['main_image'])) : ?>
                    <?php $imageSrc = wp_get_attachment_image_src( $fields['main_image']['id'], '625x420'); ?>
    			    <img class="placeholder" src="<?php echo $imageSrc[0]; ?>" width="<?php echo $imageSrc[1]; ?>" height="<?php echo $imageSrc[2]; ?>" alt="">
                <?php endif; ?>
                    
                <?php if (isset($fields['main_heading']) && !empty($fields['main_heading'])) : ?>
    			    <h3><?php echo $fields['main_heading']; ?></h3>
                <?php endif; ?>


                <?php if (isset($fields['left_block']) && !empty($fields['left_block'])) : ?>
                    <p class="left">
                    <?php foreach ($fields['left_block'] as $row) : ?>
                        <?php if (isset($row['label']) && !empty($row['label'])): ?>
                            <?php echo $row['label'] . ' '; ?>
                        <?php endif; ?>
                        <?php if (isset($row['value']) && !empty($row['value'])): ?>
                            <?php echo $row['value']; ?><br>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    </p>
                <?php endif; ?>

                <?php if (isset($fields['right_block']) && !empty($fields['right_block'])) : ?>
    			    <p class="right">
                    <?php foreach ($fields['right_block'] as $row) : ?>
                        <?php if (isset($row['label']) && !empty($row['label'])): ?>
                            <?php echo $row['label'] . ' '; ?>
                        <?php endif; ?>
                        <?php if (isset($row['value']) && !empty($row['value'])): ?>
                            <?php echo $row['value']; ?><br>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    </p>
                <?php endif; ?>

    			</div>
    			

                <?php if (isset($fields['form_email']) && !empty($fields['form_email'])) :?>
                <?php $form_nonce = wp_create_nonce("send_email_nonce"); ?>
                <?php $form_email = $fields['form_email']; ?>
                <script type="text/javascript">var formEmail = '<?php echo $fields['form_email']; ?>';</script>
    			<div id="contact-form">
    			    <h3>Send us a request</h3>
    			    <form>
    				<textarea name="text"></textarea>
                    <input id="nonce" type="hidden" value="<?php echo $form_nonce; ?>">
    				<input placeholder="Your e-mail address" id="email" type="text" name="email">
    				<button>Send</button>
    			    </form>
    			</div>
    			
    			<div id="contact-form-thanks">
    			    <h3>Thanks for sending us a request!</h3>
    			</div>
                <?php endif; ?>
		    </div>
		    
		    <div class="contacts-description">
                <?php if (isset($fields['description_image']) && !empty($fields['description_image'])) : ?>
    			    <?php $imageSrc = wp_get_attachment_image_src( $fields['description_image']['id'], '625x420'); ?>
                    <img class="placeholder" src="<?php echo $imageSrc[0]; ?>" width="<?php echo $imageSrc[1]; ?>" height="<?php echo $imageSrc[2]; ?>" alt="">
                <?php endif; ?>

                <?php if (isset($fields['description_heading']) && !empty($fields['description_image'])) : ?>
    			    <h3><?php echo $fields['description_heading']; ?></h3>
                <?php endif; ?>
                
                <?php if (isset($fields['description_content']) && !empty($fields['description_content'])) : ?>
    			    <div><?php echo $fields['description_content']; ?></div>
                <?php endif; ?>
		    </div>
		</div>
    <?php endif; ?>
<?php include('footer.php'); ?>
