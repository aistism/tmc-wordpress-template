// var MicestBaltics = new Array();
var MicestBalticsIndex = -1;

//@TODO: remove
getRandomColor = function () {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++) {
	color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
};

temp_random = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

addMicestBaltics = function () {

    MicestBalticsIndex++;
    if (MicestBalticsIndex >= MicestBaltics.length) {
        MicestBalticsIndex = 0;
    }
    showMicestBaltics();
};

showMicestBaltics = function () {

    var mb = MicestBaltics[MicestBalticsIndex];

    $(".micest-baltics-item figure img").fadeOut(300,function(){
	
	//$(this).remove();
	$('.micest-baltics-item figure img').attr('src', mb.image).fadeIn(500);
    });
    $(".micest-baltics-item-description", ".micest-baltics-item").fadeOut(300,function(){
	
	$(".micest-baltics-item-description", ".micest-baltics-item").html(mb.paragraph).fadeIn(500);	
    });

    
};

addCreamestIdeas = function (page, nonce) {
   $.ajax({
        type: "GET",
        url: "wp-admin/admin-ajax.php", 
        dataType: 'json',
        data: ({ action: 'post_load', page: page, nonce: nonce}),
        
        success: function(data){

            if (data.type == 'success') {
                $("#creamest-ideas-more").attr('data-page', data.nextPage);

                var posts = data.posts;
                var postCount = 0;
                $.each(posts, function(index, post) {
                    console.log(post);
                    var prefab  = $(".creamest-ideas-item", "#prefabs").clone();
                    prefab.find("figcaption").html(post.title);
                    prefab.find("div").html(post.content);
                    prefab.find("img").attr('src', post.img);
                   
                    $(".creamest-ideas").append(prefab);
                    
                    $(".creamest-ideas-item.hidden", "#creamiest-ideas").each(function(){
                    	$(this).fadeIn(300, function(){
                    	    $(this).removeClass("hidden");
                    	});	
                    });
                    postCount++;
                });

                if (postCount <= 1) {
                     $("#creamest-ideas-more").fadeOut();
                }

            }
                
        }
    });
};


$(function () {

    $(".menu-primary a").on("click", function (e) {

	e.preventDefault();
	//console.log($(this).attr("href"));
	$("html, body").animate({scrollTop: $($(this).attr("href")).offset().top}, 400);
    });

    $(".menu-subscribe").on("click", function (e) {

	e.preventDefault();
	$("#subscribe-form").slideDown(300);
    });

    $("#subscribe-close").on("click", function (e) {

	e.preventDefault();
	$("#subscribe-form").slideUp(200);
    });

    $("#micest-baltics-navigation-previous").on("click", function (e) {

	e.preventDefault();

	if (MicestBaltics.length < 2)
	    return;

	if (MicestBalticsIndex === 0)
	    MicestBalticsIndex = MicestBaltics.length - 1;
	else
	    MicestBalticsIndex--;

	showMicestBaltics();
    });

    $("#micest-baltics-navigation-next").on("click", function (e) {

	e.preventDefault();
	addMicestBaltics();
    });

    $("#creamest-ideas-more").on("click", function (e) {
        var nonce = $(this).data('nonce');
        var page = $(this).attr('data-page');

        addCreamestIdeas(page, nonce);
    });
    
    $("button", "#contact-form").on("click", function (e) {
    
        e.preventDefault();

        var formData = [{
            text: $('#contact-form form textarea').val(),
            email: $('#contact-form form input#email').val()
        }];
        var nonce = $('#contact-form form input#nonce').val();
        var emailTo = formEmail;
        $.ajax({
            type: "GET",
            url: "wp-admin/admin-ajax.php", 
            dataType: 'json',
            data: ({ action: 'send_email', nonce: nonce, data: formData, emailTo: emailTo}),
            
            success: function(data){
                    
                $("#contact-form").fadeOut(300, function(){
                    
                    $("#contact-form-thanks").fadeIn(400);
                });
            }
        });
        
    });    
    $("button", "#subscribe-form").on("click", function (e) {
	
    	e.preventDefault();

        var formData = [{
            email: $('#subscribe-form form input#email').val()
        }];
        var nonce = $('#subscribe-form form input#nonce').val();
        var emailTo = formEmail;
        $.ajax({
            type: "GET",
            url: "wp-admin/admin-ajax.php", 
            dataType: 'json',
            data: ({ action: 'subscribe_email', nonce: nonce, data: formData, emailTo: emailTo}),
            
            success: function(data){     
            	$("#subscribe-form").slideUp(200);
            }
        });
    	
    });
});