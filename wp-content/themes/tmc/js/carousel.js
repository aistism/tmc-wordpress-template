var carousel;
var carouselActiveItemIndex = 0;
var carouselFadeTime = 600;
var carouselChangeTime = 6000;
var carouselImages = 0;
var carouselLoadedImages = 0;

function carouselInit() {
    
    carouselActiveItemIndex = 0;    
    
    if (carousel > 0)
	clearInterval(carousel);
    
    carouselImages = $(".slideshow-items img").size();
    carouselLoadedImages = 0;
    
    $(".slideshow-items img").each(function(){
	
	$(this).unveil(0, function() {
	    
	    $(this).load(function() {

		var carouselItem = $(this);
		
		if (carouselItem.index() === 0) {

		    carouselItem.fadeIn(400, function(){
			$(this).addClass("active");
		    });
		}

		//$(this).closest(".carousel-item").css("background-image", "url(" + $(this).attr("src") + ")");
		//$(this).remove();
		carouselLoadedImages += 1;
		carouselStart();
	    });
	});
    });
    $(".slideshow-items img").trigger('unveil');
}

// Starts carousel when all carousel images are loaded.
function carouselStart() {
    
    if (carouselLoadedImages === carouselImages) {
	
	carousel = setInterval(function(){

	    carouselNext();

	}, carouselChangeTime);	
    }
}

function carouselNext() {

   var itemIndex = 0;
    
   if ( carouselActiveItemIndex === $(".slideshow-items img").size() -1) {
       
       itemIndex = 0;
   }
   else {
       
       itemIndex = carouselActiveItemIndex +1;
   }
   
   carouselDisplayItem(itemIndex);
}

function carouselDisplayItem(itemIndex) {
    
   $(".slideshow-items img")
	.eq(carouselActiveItemIndex)
	.fadeOut(carouselFadeTime)
	.removeClass("active");

   $(".slideshow-items img")
	.eq(itemIndex)
	.fadeIn(carouselFadeTime)
	.addClass("active");
   
   carouselActiveItemIndex = itemIndex;    
}

$(function(){
    carouselInit();
});