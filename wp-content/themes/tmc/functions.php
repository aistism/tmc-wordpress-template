<?php


//Register image size formats
add_image_size( '850x550', 850, 550 );

add_image_size( '400x250', 400, 250 );

add_image_size( '625x420', 625, 420 );

add_image_size( '290x300', 290, 300 );

add_image_size( '850x565', 850, 565 );

add_image_size( '625x420', 625, 420 );



//Menu init
function register_main_menu() {
  register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_main_menu' );

add_action("wp_ajax_post_load", "post_load");
add_action("wp_ajax_nopriv_post_load", "post_load");
function post_load() {

    $result['type'] = 'ok';
    if ( !wp_verify_nonce( $_REQUEST['nonce'], "post_load_nonce")) {
        $result['type'] = "error_invalid_nonce";
    }   

    $posts = get_posts(array(
        'post_type'        => 'post',
        'posts_per_page'   => 2,
        'paged'            => $_REQUEST["page"],
        'post_status'      => 'publish',
    ));

    if($posts === null) {
        $result['type'] = "error_post_not_found";
    }
    else {
        $result['type'] = "success";
        $result['nextPage'] = $_REQUEST["page"] + 1;
        $result['posts'] = array();

        foreach ($posts as $post) {
            $post_fields = get_fields($post->ID);
            $title = $post->post_title;

            $imageObj = wp_get_attachment_image_src( $post_fields['post_image']['id'], '625x420');
            $imageSrc = $imageObj[0];

            $content = apply_filters ("the_content", $post->post_content);

            $post_array = array(
                'title' => $title,
                'img' => $imageSrc,
                'content' => $content
            );
            array_push($result['posts'], $post_array);
        }
    }

    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    }
    else {
        header("Location: ".$_SERVER["HTTP_REFERER"]);
    }

    die();

}


add_action("wp_ajax_send_email", "send_email");
add_action("wp_ajax_nopriv_send_email", "send_email");

function send_email() {
    $result['type'] = 'ok';
    if ( !wp_verify_nonce( $_REQUEST['nonce'], "send_email_nonce")) {
        $result['type'] = "error_invalid_nonce";
    }   

    if (isset($_REQUEST['emailTo']) && !empty($_REQUEST['emailTo'])) {
        if (isset($_REQUEST['data'][0]['email']) && !empty($_REQUEST['data'][0]['email'])) {
            if (isset($_REQUEST['data'][0]['text']) && !empty($_REQUEST['data'][0]['text'])) {
                $headers = array(
                    'From' => $_REQUEST['data'][0]['email']
                );
                $result['email_status'] = wp_mail( $_REQUEST['emailTo'], 'Micest Baltics Contact form', $_REQUEST['data'][0]['text'], $headers);
            }
        }
    }


    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    }
    else {
        header("Location: ".$_SERVER["HTTP_REFERER"]);
    }

    die();
}

add_action("wp_ajax_subscribe_email", "subscribe_email");
add_action("wp_ajax_nopriv_subscribe_email", "subscribe_email");

function subscribe_email() {
    $result['type'] = 'ok';

    if (isset($_REQUEST['emailTo']) && !empty($_REQUEST['emailTo'])) {
        if (isset($_REQUEST['data'][0]['email']) && !empty($_REQUEST['data'][0]['email'])) {
            $headers = array(
                'From' => $_REQUEST['data'][0]['email']
            );
            $result['email_status'] = wp_mail( $_REQUEST['emailTo'], 'Micest Baltics Subscribtion', 'Subscriber email: ' . $_REQUEST['data'][0]['email'], $headers);
        }
    }


    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    }
    else {
        header("Location: ".$_SERVER["HTTP_REFERER"]);
    }

    die();
}

?>